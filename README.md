project
-------

 - clonar el proyecto "git clone
   https://robinsluna@bitbucket.org/robinsluna/status.git"
 - ubicarse en la carperta root del proyecto "status"
 - descargar composer "https://getcomposer.org/download/"
 

>     php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
>     php composer-setup.php
>     php -r "unlink('composer-setup.php');"

 - verificar que la instalacion fue exitosa nuevo archivo
   "composer.phar"
 - ejecutar el comando "php composer.phar update" una vez finalizado
   este proceso automaticamente le pedira los datos de configuracion del proyecto
 - configurar el proyecto en el archivo "app/config/parameters.yml
>     parameters:
>     		database_driver: pdo_pgsql
>     		database_host: 127.0.0.1
>     		database_port: null
>     		database_name: project
>     		database_user: postgres
>     		database_password: 1234
>     		mailer_transport: smtp
>     		mailer_host: smtp.gmail.com
>     		mailer_user: email@gmail.com
>     		mailer_password: password_email
>     		secret: f7a35d6906b7ecb12253038c8875b818c9fbb96e

 - ubicarse en la carperta root del proyecto "status"		
 - crear la base de datos "php bin\console doctrine:database:create"
 - crear el schema "php bin\console doctrine:schema:create"
 - (OPCIONAL)cargar datos de iniciales "php bin/console doctrine:fixtures:load"
 - dar permisos a las carpetas cache y logs "chmod -R 775 var/*"


leer [documentacion de symfony](http://symfony.com/doc/current/setup.html)
visita [http://intraway.yoonline.info/](http://intraway.yoonline.info/)