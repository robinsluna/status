<?php

namespace StatusBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use StatusBundle\Entity\Status;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @RouteResource("status" , pluralize=false)
 */
class StatusController extends FOSRestController
{

    /**
     * @ApiDoc(
     *  section="Status",
     *  resource=true,
     *  description="this endpoint is used to retreive status messages, it will get status messages paginated. By default, it will retrrieve 20 items, sorted by date, newers first.",
     *  filters={
     *      {"name"="p", "dataType"="integer"},
     *      {"name"="r", "dataType"="integer"},
     *      {"name"="q", "dataType"="string"}
     *  }
     * )
     */
    public function cgetAction(Request $request)
    {
        $limit         = $request->get("r", NULL);
        $offset        = $request->get("p", NULL);
        $status        = $request->get("q", NULL);
        $api_doc_url   = $this->generateUrl('nelmio_api_doc_index', array(),
            UrlGeneratorInterface::ABSOLUTE_URL);
        $error_message = NULL;
        if ($limit && !filter_var($limit, FILTER_VALIDATE_INT)) {
            $error_message = "invalid number of rows";
        }
        if ($offset && !filter_var($offset, FILTER_VALIDATE_INT)) {
            $error_message = "invalid number of page";
        }
        if ($error_message) {
            return $this->handleView($this->view(["code" => 400001, "message" => $error_message,
                        "link" => $api_doc_url], 400));
        }
        $em   = $this->getDoctrine()->getManager();
        $data = $em
            ->getRepository('StatusBundle:Status')
            ->findByPublishCustom($limit, $offset, $status);
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *  section="Status",
     *  description="This endpoint its used to publish a new status message,the messages can be either, owned by someone, or be an annon. status messages annon statuses are send with 'annonymus' as value in email.if an email address is received, an e-mail will be sent with a code to validate ownership of the message. the message will be published after a succesfull validation",
     *  output="StatusBundle\Entity\Status",
     *  statusCodes={
     *      201="Created",
     *      400={"missing email addres", "Status value should not be blank.","Status value should not be null."}
     *  }
     * )
     */
    public function postAction(Request $request)
    {
        $data      = $request->request->all();
        $status    = isset($data['status']) ? $data['status'] : NULL;
        $email     = isset($data['email']) ? $data['email'] : NULL;
        $entity    = new Status();
        $entity->setStatus($status);
        $entity->setEmail($email);
        $validator = $this->get('validator');
        $errors    = $validator->validate($entity);
        if (count($errors) > 0) {
            $apiDocUrl    = $this->generateUrl('nelmio_api_doc_index', array(),
                UrlGeneratorInterface::ABSOLUTE_URL);
            $entityErrors = array();
            foreach ($errors as $value) {
                $entityErrors[] = ["code" => 400000, "message" => $value->getMessage(),
                    "link" => $apiDocUrl];
            }
            return $this->handleView($this->view($entityErrors, 400));
        }
        if (!$entity->getEmail()) {
            $entity->setPublish(TRUE);
        } else {
            $entity->generateCode();
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        if ($entity->getCode()) {
            $errors = $this->sendEmail($entity);
            if ($errors) {
                $em->remove($entity);
                $em->flush();
                return $this->handleView($this->view($errors, 400));
            }
        }
        return $this->handleView($this->view($entity->getData(), 200));
    }

    /**
     * @ApiDoc(
     *  section="Status",
     *  description="Deletes the status message, it will also send an email with a link que te confirm the operation",
     *  requirements={
     *      {"name"="id","dataType"="integer", "requirement"="\d+"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      400="annon statuses cannot be deleted",
     *      404={"status messge not found"}
     *  }
     * )
     */
    public function deleteAction($id)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('StatusBundle:Status')->find($id);
        if ($entity) {
            $data = ["email" => $entity->getUser()];
            if (!$entity->getEmail()) {
                $em->remove($entity);
                $em->flush();
            } else {
                $entity->generateCode(TRUE);
                $em->persist($entity);
                $em->flush();
                $errors = $this->sendEmail($entity);
                if ($errors) {
                    return $this->handleView($this->view($errors, 400));
                }
            }
            return $this->handleView($this->view($data, 200));
        } else {
            $apiDocUrl = $this->generateUrl('nelmio_api_doc_index', array(),
                UrlGeneratorInterface::ABSOLUTE_URL);
            return $this->handleView($this->view(["code" => 400000, "message" => "status messge not found",
                        "link" => $apiDocUrl], 404));
        }
    }

    /**
     * @ApiDoc(
     *  section="Status",
     *  description="gets an status message by its id",
     *  requirements={
     *      {"name"="id","dataType"="integer", "requirement"="\d+"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404={"status messge not found"}
     *  }
     * )
     */
    public function getAction($id)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('StatusBundle:Status')->find($id);
        if ($entity) {
            return $this->handleView($this->view($entity->getData(), 200));
        } else {
            $apiDocUrl = $this->generateUrl('nelmio_api_doc_index', array(),
                UrlGeneratorInterface::ABSOLUTE_URL);
            return $this->handleView($this->view(["code" => 400000, "message" => "status messge not found",
                        "link" => $apiDocUrl], 404));
        }
    }

    /**
     * @ApiDoc(
     *  section="Status",
     *  description="endpoint for the user to validate de received code after publishing or requesting deletion of a status message",
     *  requirements={
     *      {"name"="id","dataType"="integer", "requirement"="\d+"},
     *      {"name"="code","dataType"="string"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404={"status messge not found"}
     *  }
     * )
     * 
     */
    public function getConfirmationAction($id, $code)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('StatusBundle:Status')
            ->findOneBy(['id' => $id, 'code' => $code]);
        if ($entity) {
            $codeArray = str_split($code);
            if ($codeArray[0] == 'D') {
                $data = ["email" => $entity->getUser()];
                $em->remove($entity);
                $em->flush();
                return $this->handleView($this->view($data, 200));
            } else if ($codeArray[0] == 'P') {
                $entity->setCode(NULL);
                $entity->setPublish(TRUE);
                $em->persist($entity);
                $em->flush();
                return $this->handleView($this->view($entity->getData(), 200));
            }
        } else {
            $apiDocUrl = $this->generateUrl('nelmio_api_doc_index', array(),
                UrlGeneratorInterface::ABSOLUTE_URL);
            return $this->handleView($this->view(["code" => 400000, "message" => "status messge not found",
                        "link" => $apiDocUrl], 404));
        }
    }

    private function sendEmail($entity)
    {
        try {
            $body    = $this->container
                ->get('templating')
                ->render("StatusBundle:Email:email.html.twig", compact("entity"));
            $message = \Swift_Message::newInstance()
                ->setSubject('Code Status')
                ->setFrom($this->container->getParameter('mailer_user'))
                ->setBcc($entity->getEmail())
                ->setContentType("text/html")
                ->setBody($body)
            ;
            $this->container->get('mailer')->send($message);
        } catch (\Exception $exc) {
            return $exc->getMessage();
        }
    }
}