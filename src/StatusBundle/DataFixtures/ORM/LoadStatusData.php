<?php

namespace StatusBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use StatusBundle\Entity\Status;

class LoadStatusData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $array = array(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "Aliquam imperdiet fringilla libero non imperdiet.",
            "Morbi in nulla sit amet nunc vestibulum efficitur.",
            "Quisque porta ultricies feugiat.",
            "Aenean congue convallis mi nec aliquam.",
            "Morbi nec ultricies mauris.",
            "Duis imperdiet, arcu viverra gravida maximus, libero nunc ultrices mauris, id sodales nulla justo at turpis.",
            "Donec in ullamcorper nibh.",
            "Donec nec malesuada nunc. Duis posuere sem in arcu fringilla, eu dignissim erat finibus.",
            "Pellentesque eu dapibus dui, ut lacinia metus."
        );
        foreach ($array as $key => $value) {
            $entity = new Status();
            $entity->setPublish(TRUE);
            if ($key < 5) {
                $entity->setEmail('ing.robins@gmail.com');
            }
            $entity->setStatus($value);
            $manager->persist($entity);
            $manager->flush();
            // >= 5 user annonymus
            $this->addReference("status-$key", $entity);
        }
    }

    public function getOrder()
    {
        return 1;
    }
}