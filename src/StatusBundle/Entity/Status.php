<?php

namespace StatusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Status
 */
class Status
{
    // ************* MODIFICATION

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return ($this->email ? $this->email : 'annonymus' );
    }

    public function getData()
    {
        return ["id" => $this->id, "email" => $this->getUser(), "status" => $this->status,
            "created_at" => $this->createdAt->format('c')];
    }

    public function generateCode($delete = FALSE)
    {
        $this->code = ($delete ? 'D' : 'P' ).
            '-'.uniqid(rand(10000, 99999));
    }

    public function __toString()
    {
        return (string) ($this->id ? $this->id : "");
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->publish   = is_null($this->publish) ? FALSE : $this->publish;
    }
    // ************* END MODIFICATION
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $code;

    /**
     * @var boolean
     */
    private $publish;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $id;

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Status
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Status
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set publish
     *
     * @param boolean $publish
     *
     * @return Status
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Status
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}