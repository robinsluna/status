<?php

namespace StatusBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use StatusBundle\DataFixtures\ORM\LoadStatusData;

class StatusControllerTest extends WebTestCase
{
    private $client;
    private $container;
    private $em;
    private $fixture;

    public function setUp()
    {
        $this->client    = static::createClient();
        $this->container = $this->client->getContainer();
        $doctrine        = $this->container->get('doctrine');
        $this->em        = $doctrine->getManager();
        $loader          = new Loader();
        $loader->addFixture(new LoadStatusData);
        $purger          = new ORMPurger($this->em);
        $executor        = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures());
        $this->fixture   = $loader->getFixtures()[0];
        parent::setUp();
    }

    protected function assertJsonResponse($response, $statusCode = 200)
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(), $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }

    public function testPostAction()
    {
        $route    = $this->container->get('router')->generate('post_status');
        $this->client->request(
            'POST', $route, array(), array(),
            array('CONTENT_TYPE' => 'application/json'), '{"status":"TESTING"}'
        );
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response, 200);
    }

    public function testGetAction()
    {
        $status = $this->fixture->getReference('status-1');

        $route    = $this->container->get('router')->generate('get_status',
            array('id' => $status->getId()));
        $this->client->request('GET', $route,
            array('ACCEPT' => 'application/json'));
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 200);
        $this->assertEquals(json_encode($status->getData()), $content);
    }

    public function testDeleteAction()
    {
        $status = $this->fixture->getReference('status-5');

        $route    = $this->container->get('router')->generate('delete_status',
            array('id' => $status->getId()));
        $this->client->request('DELETE', $route,
            array('ACCEPT' => 'application/json'));
        $response = $this->client->getResponse();
        $content  = $response->getContent();
        $this->assertJsonResponse($response, 200);
        $this->assertEquals(json_encode(['email' => $status->getUser()]),
            $content);
    }
}